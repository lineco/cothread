﻿/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef _COMMAND_H_
#define _COMMAND_H_

typedef struct command
{
    char name[12];
    int (*fun)(int argc,char **argv);
}command_t;

extern const command_t command_table[];
extern const int command_nr;

#endif  // _COMMAND_H_

