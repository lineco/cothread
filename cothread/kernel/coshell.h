﻿/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef	_SHELL_H_
#define	_SHELL_H_

void create_shell_thread(void);
void shell_check_input(void);

/* 这几个函数由硬件驱动提供 */
int read_flag(void);
int read_data(void);

#endif	// _SHELL_H_
