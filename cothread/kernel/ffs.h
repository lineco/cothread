/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef ___FFS_H_
#define ___FFS_H_

/**
 * ffs - find first bit in word.
 * @word: The word to search
 *
 * Undefined if no bit exists, so code should check against 0 first.
 */
static unsigned int __ffs(unsigned int word)
{
    int num = 0;

    if ((word & 0xffff) == 0) 
    {
        num += 16;
        word >>= 16;
    }
    
    if ((word & 0xff) == 0) 
    {
        num += 8;
        word >>= 8;
    }
    
    if ((word & 0xf) == 0) 
    {
        num += 4;
        word >>= 4;
    }
    
    if ((word & 0x3) == 0) 
    {
        num += 2;
        word >>= 2;
    }
    
    if ((word & 0x1) == 0)
        num += 1;
    
    return num;
}
#endif // ___FFS_H_